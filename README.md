Installation
--------------
Install the following applications:

* stud.io studio
* LPub3D
* LDView
* LDDraw

Directory Structure
-------------------
```bash
Library # Adobe Illustrator swatches file(s)
ldr     # The ldr files exported from stud.io
pdf     # The pdf files exported from LPub3D and Adobe Illustrator
specs   # LEGO specifications (e.g, color palette)
src     # LEGO model files created by stud.io
```

Process
-----------
* Create inventory in stud.io studio
* Export inventory to ldr
* Import ldr in LPub3D
* Add BOM
* Export BOM to PDF 
* Open PDF in Illustrator (scale down to 50% in case of full inventory)
* Layout the parts
* Save PDF and print
